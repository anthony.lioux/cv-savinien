<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
  <link rel="stylesheet" type="text/css" href="library/owl.carousel/assets/owl.carousel.min.css">
  <link rel="stylesheet" href="style.css">
  <link href="library/aos/aos.css" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">
  <title>Savinien Monteil</title>
</head>

<body>
  <!-- mobile menu -->
  <button class="ham"></button>
  <nav class="navbar">
    <ul class="menuHidden">
      <li><a class="menuLink" href="#home">Accueil</a></li>
      <li><a class="menuLink" href="#tech">Compétences</a></li>
      <li><a class="menuLink" href="#xp">Expériences</a></li>
      <li><a class="menuLink" href="#grade">Formations</a></li>
      <li><a class="menuLink" href="#infoComp">Infos</a></li>
      <li><a class="menuLink" href="#contact">Contact</a></li>
    </ul>
  </nav>


  <div class="menu">
    <a href="#home">
      <h1 class="btnMenu btn1">Accueil</h1>
    </a>
    <a href="#tech">
      <h1 class="btnMenu btn1">Compétences</h1>
    </a>
    <a href="#xp">
      <h1 class="btnMenu btn2">Expériences</h1>
    </a>
    <a href="#grade">
      <h1 class="btnMenu btn3">Formations</h1>
    </a>
    <a href="#infoComp">
      <h1 class="btnMenu btn4">Infos</h1>
    </a>
    <a href="#contact">
      <h1 class="btnMenu btn4">Contact</h1>
    </a>


  </div>

  <div id="home" class="homePage">
    <div id="particles-js"></div>
    <script type="text/javascript" src="particles.js"></script>
    <script type="text/javascript" src="app.js"></script>
    <div class="posteNom">

      <h1>Savinien Monteil</h1>
      <div class="meter">
        <span style="width:100%"><span class="prog"></span></span>
      </div>
      <h1>Développeur Web et Mobile </h1>
    </div>
    <div class="profilPic">
      <audio id="audio" src="sounds/rick-roll-music.mp3"></audio>
      <a href="#" id="play">
        <img src="img/profil.jpg" class="profil" id="change-image"></a>
    </div>
    <div class="quoteReseaux">
      <div class="reseaux">
        <a href="https://www.linkedin.com/in/savinien-monteil/">
          <i class="fab fa-linkedin"></i></a>
        <a href="https://portefolio.savinien-monteil.labo-ve.fr/">
          <i class="fas fa-wallet"></i></a>
      </div>

      <div class="quote">
        <h3 class="homeQuote">Passionné par le monde du numérique depuis l'enfance, je réalise actuellement une formation
          professionnelle de développeur Web et Mobile.
          Je découvre ainsi le monde du code avec beaucoup d'intérêts.</h3>
      </div>
      <div class="reseaux">
        <a href="https://github.com/savinienm">
          <i class="fab fa-github"></i>
        <a href="CV/CV-Savinien-Monteil.pdf" download>
          <i class="fas fa-file-download"></i></a>
      </div>
    </div>
  </div>

  <div id="tech" class="technos">
    <h1 class="margT" data-aos="zoom-in-up" style="color:black;">Compétences</h1>
    <h2 class="margT" data-aos="zoom-in-up" style="color:#02b4a2;">Hard skills</h2>

    <div class="hardSkill">
      <ul class="hard">
        <li><i class="fas fa-angle-double-right"></i> Développer la partie front-end d’une application</li>
        <li><i class="fas fa-angle-double-right"></i> Maquetter une application</li>
        <li><i class="fas fa-angle-double-right"></i> Réaliser un interface utilisateur dynamique et adaptable</li>
      </ul>
      <ul class="skill">
        <li><i class="fas fa-angle-double-right"></i> Développer la partie back-end d’une application</li>
        <li><i class="fas fa-angle-double-right"></i> Créer une base de données</li>
        <li><i class="fas fa-angle-double-right"></i> Intégrer les recommandations de sécurité</li>
      </ul>
    </div>
    <h2 class="margT mobT" data-aos="zoom-in-up" style="color:#02b4a2;">Technologies</h2>

    <div class="mobileTech">
      <ul>
        <li>
          <p class="size3">Python</p>
        </li>
        <li>
          <p class="size2">JavaScript</p>
        </li>
        <li>
          <p class="size5">Jquery</p>
        </li>
        <li>
          <p class="size2">PHP</p>
        </li>
        <li>
          <p class="size7">Java</p>
        </li>
        <li>
          <p class="size3">React</p>
        </li>
        <li>
          <p class="size4">Symfony</p>
        </li>
        <li>
          <p class="size6">Angular</p>
        </li>
        <li>
          <p class="size1">CSS</p>
        </li>
        <li>
          <p class="size5">Bootstrap</p>
        </li>
        <li>
          <p class="size1">HTML</p>
        </li>
        <li>
          <p class="size3">MySQL</p>
        </li>
        <li>
          <p class="size4">MariaDB</p>
        </li>
        <li>
          <p class="size7">C#</p>
        </li>

      </ul>
    </div>
    <div class="animTech">
      <!-- first page of tiles -->
      <!-- three rows in full page -->
      <div class="three rows" id="first-page">
        <!-- three tiles per row -->
        <div class="three cols">
          <!-- tile plus some schmancyness -->
          <div class="schmancy tile pic0 html">
            <!-- tile content -->
            <div class="content"> HTML </div>
          </div>
          <!-- and so on... -->
          <div class="schmancy tile pic3 css">
            <div class="content"> CSS </div>
          </div>
          <div class="schmancy tile pic1 php">
            <div class="content"> PHP </div>
          </div>
        </div>
        <div class="four cols">
          <div class="schmancy tile pic2 js">
            <div class="content"> JS </div>
          </div>
          <div class="schmancy tile pic1 symfony">
            <div class="content"> Symfony </div>
          </div>
          <div class="schmancy tile pic2 react">
            <div class="content"> React </div>
          </div>
          <div class="schmancy tile pic1 mysql">
            <div class="content"> MySQL </div>
          </div>
        </div>
        <div class="three cols">
          <div class="schmancy tile pic0 jquery">
            <div class="content"> jQuery </div>
          </div>
          <div class="schmancy tile pic3 bootstrap">
            <div class="content"> Bootstrap </div>
          </div>
          <div class="schmancy tile pic1 java">
            <div class="content"> Java </div>
          </div>
        </div>
        <div class="four cols">
          <div class="schmancy tile pic2 angular">
            <div class="content"> Angular </div>
          </div>
          <div class="schmancy tile pic1 mariadb">
            <div class="content"> MariaDB </div>
          </div>
          <div class="schmancy tile pic2 csharp">
            <div class="content"> C# </div>
          </div>
          <div class="schmancy tile pic1 python">
            <div class="content"> Python </div>
          </div>
        </div>
      </div>
    </div>


    <h2 class="margT" data-aos="zoom-in-up" style="color:#02b4a2;">Soft skills</h2>
    <div class="softSkills">
      <ul class="soft">
        <li><i class="fas fa-angle-double-right"></i> Autonomie</li>
        <li><i class="fas fa-angle-double-right"></i> Adaptabilité</li>
        <li><i class="fas fa-angle-double-right"></i> Pédagogie</li>
      </ul>
      <ul class="skills">
        <li><i class="fas fa-angle-double-right"></i> Esprit d'Équipe</li>
        <li><i class="fas fa-angle-double-right"></i> Méthodologie</li>
        <li><i class="fas fa-angle-double-right"></i> Gestion de projet</li>
      </ul>
    </div>

  </div>

  <div id="xp" class="experiences">
    <h1 class="margT margX" data-aos="zoom-in-up">Expériences</h1>
    <div class="container">

      <div class="timeline-item white" date-is='2019' data-aos="fade-up">
        <h2 class="xpT white">Médiation Scientifique</h2>
        <p class="white">
          Médiation scientifique en ornithologie tout public et conception de trames de visites et d'ateliers
          pédagogiques au Parc des Oiseaux (CCD de 9 mois).
        </p>
      </div>

      <div class="timeline-item white" date-is='2018' data-aos="fade-up">
        <h2 class="xpT white">Conception Pédagogique</h2>
        <p class="white">
          Conception d'exposition en astrophysique, réalisation de podcasts, encadrement de projet pollinisation à
          l'OSU Pythéas (stage de 6 mois).
        </p>
      </div>

      <div class="timeline-item white" date-is='2017' data-aos="fade-up">
        <h2 class="xpT white">Conception et Médiation Scientifique</h2>
        <p class="white">
          Conception et animation d'ateliers pour la Fête de la Science avec l'OSU Pythéas.<br>
          Médiation scientifique à l'association d'éducation populaire et scientifique Ebulliscience (CDD de 2
          mois).<br>
          Conception d'une exposition "dont vous êtes le héros" à l'OSU Pythéas (stage de 13 semaines).
        </p>
      </div>
      <div class="timeline-item white" date-is='2016' data-aos="fade-up">
        <h2 class="xpT white">Conception et Médiation Scientifique</h2>
        <p class="white">
          Conception d'animation (thème de la sixième extinction) dans le service de médiation de la Cité des Sciences
          et de l'Industrie (stage de 3 mois).
        </p>
      </div>
      <div class="timeline-item white" date-is='2015' data-aos="fade-up">
        <h2 class="xpT white">Éducation à l'environnement</h2>
        <p class="white">
          Animation et éducation à l'environnement de colonies et classes vertes (CDD de 4 mois).
        </p>
      </div>
      <div class="timeline-item white" date-is='2013-2014' data-aos="fade-up">
        <h2 class="xpT white">Éducation à l'environnement</h2>
        <p class="white">
          Education à l'environnement à l'association "Le Croux" (stage en alternance de 4 mois).<br>
          Création et réalisation d'un projet de stage dans une ferme vivrière au Népal (1 mois).</p>
        </p>
      </div>
      <div class="timeline-item white" date-is='2011-2012' data-aos="fade-up">
        <h2 class="xpT white">Service Civique</h2>
        <p class="white">
          Organisation d'actions sociales dans le cadre d'échanges franco-sénégalais (service civique à "Unis Cité" de
          9 mois).
        </p>
      </div>
      <div class="timeline-item white" date-is='2013-2017' data-aos="fade-up">
        <h2 class="xpT white">Animations pédagogiques et ludiques</h2>
        <p class="white">
          Multiples expériences d'animations durant les vacances scolaires.
        </p>
      </div>
    </div>
  </div>

  <div id="grade" class="graduation">
    <h1 class="margT" data-aos="zoom-in-up" style="color:black;">Diplômes</h1>

    <div class="container">

      <div class="timeline-item1" date-is='2020-2021' data-aos="fade-up">
        <h2 class="gT">Titre Professionnel</h2>
        <p>
        Développeur Web et Web Mobile - Formation Simplon Le Cheylard - équivalent bac+2.
        </p>
      </div>
      
      <div class=" timeline-item1" date-is='2016-2018' data-aos="fade-up">
          <h2 class="gT">Master</h2>
        <p>
          Master Médiation en Environnement et Communication Scientifique - mention bien.
        </p>
      </div>
      
      <div class=" timeline-item1" date-is='2015-2016' data-aos="fade-up">
            <h2 class="gT">Licence Professionnelle</h2>
        <p>
          Licence Professionnelle Médiation Scientifique et Education à l'Environnement - mention bien.
        </p>
      </div>
        <div class=" timeline-item1" date-is='2012-2014' data-aos="fade-up">
              <h2 class="gT">BTS</h2>
              <p>
                BTS Gestion et Protection de la Nature.
              </p>
      </div>
      <div class="timeline-item1" date-is='2010-2011' data-aos="fade-up">
        <h2 class="gT">Licence</h2>
        <p>
          L1 de Psychologie.
        </p>
      </div>
      <div class="timeline-item1" date-is='2008-2010' data-aos="fade-up">
        <h2 class="gT">Baccalauréat</h2>
        <p>
          Baccalauréat STAV.
        </p>
      </div>
    </div>
  </div>
    <div id="infoComp" class="hobby">

      <h1 class="margT" data-aos="zoom-in-up">Infos complémentaires</h1>
      <h2 class="white" data-aos="fade-up">Centres d'intérêt</h2>
      <p class="curiousQuote white">Curieux par nature, je m'intéresse à de multiples sujets et activités</p>

      <div class="interestList">
        <div class="intL">
          <i class="gap fas fa-camera"></i>
          <h4 class="white">La photographie</h4>
          <i class="gap fas fa-leaf"></i>
          <h4 class="white">La nature</h4>
          <i class="gap fas fa-hiking"></i>
          <h4 class="white">La randonnée</h4>
        </div>
        <div class="intG">
          <i class="gap fas fa-microscope"></i>
          <h4 class="white">Les sciences</h4>
          <i class="gap fas fa-dice"></i>
          <h4 class="white">Les jeux de société</h4>
          <i class="gap fas fa-laptop"></i>
          <h4 class="white">Le numérique</h4>

        </div>
      </div>
      <h2 class="white" data-aos="fade-up">Quelques exemples de mes photographies</h2>
      <div class="owl-carousel owl-theme">
        <img class="item" src="img/Carousel-img/DSC_1148.jpg" alt="1">
        <img class="item" src="img/Carousel-img/DSC_1582.jpg" alt="2">
        <img class="item" src="img/Carousel-img/DSC_1634.jpg" alt="3">
        <img class="item" src="img/Carousel-img/DSC_1822.jpg" alt="4">
        <img class="item" src="img/Carousel-img/DSC_1839.jpg" alt="5">
        <img class="item" src="img/Carousel-img/DSC_2091.jpg" alt="6">
      </div>
      <div class="language">
        <h2  class="white"data-aos="zoom-in-up">Langues</h2>
        <div class="lang" data-aos="fade-up">
          <p class="white">Français</p>
          <div class="progress">
            <div class="progress-value"></div>
          </div>

          <p class="white">Anglais</p>
          <div class="progress2">
            <div class="progress-value2"></div>
          </div>
          <p class="white">Allemand</p>
          <div class="progress3">
            <div class="progress-value3"></div>
          </div>

          <p class="white">Japonais</p>
          <div class="progress4">
            <div class="progress-value4"></div>
          </div>
        </div>
      </div>

      <h2 class="white" data-aos="zoom-in-up">Compétences annexes</h2>
      <div class="compAnn" data-aos="fade-up">
        <p class="white">Illustrator</p>
        <div class="progress5">
          <div class="progress-value5"></div>
        </div>

        <p class="white">Photoshop</p>
        <div class="progress6">
          <div class="progress-value6"></div>
        </div>
      </div>
    </div>

    <h1 class="margT margC" data-aos="zoom-in-up" style="color:black;">Contact</h1>
    <div id="contact" class="contact">
      <div class="reseaux2" data-aos="zoom-in-up">
        <div class="align">
          <a href="https://www.linkedin.com/in/savinien-monteil/">
            <div class="labelReseaux">
              <i class="fab fa-linkedin" style="color: #02b4a2;"></i>
          </a>
        </div>
        <p>Retrouvez moi sur Linkedin</p>
      </div>
      <div class="align">
        <a href="https://portefolio.savinien-monteil.labo-ve.fr/">
          <div class="labelReseaux">
            <i class="fas fa-wallet" style="color: #02b4a2;"></i>
        </a>
      </div>
      <p>Rendez-vous sur mon portefolio</p>
    </div>
    <div class="align">
      <a href="https://github.com/savinienm">
        <div class="labelReseaux">
          <i class="fab fa-github" style="color: #02b4a2;"></i>
      </a>
    </div>
    <p>Aller sur mon Github</p>
  </div>
  <div class="align">
    <a href="CV/CV-Savinien-Monteil.pdf" download>
      <div class="labelReseaux">
        <i class="fas fa-file-download" style="color: #02b4a2;"></i>
    </a>
  </div>
  <p>Télécharger mon CV</p>
  </div>
  </div>

  <form action="index.php" method="post">

    <div class="formcont" data-aos="fade-down">
      <?php
      if (empty($_POST)) : ?>
        <label>Prénom</label>
        <br />
        <input class="input" type="text" name="prenom" pattern="[A-zÀ-ÖØ-öø-ÿ- ]{2,}" maxlength="50" required />
        <br />
        <label>Nom</label>
        <br />
        <input class="input" type="text" name="nom" pattern="[A-zÀ-ÖØ-öø-ÿ- ]{2,}" maxlength="50" required />
        <br />
        <label>Entreprise</label>
        <br />
        <input class="input" type="text" name="entreprise" pattern="[A-zÀ-ÖØ-öø-ÿ0-9- ]{2,}" maxlength="50" />
        <br />
        <label>Fonction dans l'entreprise</label>
        <br />
        <input class="input" type="text" name="fonction" pattern="[A-zÀ-ÖØ-öø-ÿ- ]{2,}" maxlength="50" />
        <br />
        <label>Téléphone</label>
        <br />
        <input class="input" type="tel" name="telephone" pattern="[0]{1}[1-7]{1}[0-9]{8}" />
        <br />
        <label>E-mail</label>
        <br />
        <input class="input" type="email" name="mail" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" minlength="7" required />
        <br />
        <label>Objet du message</label>
        <br />
        <input class="input" type="text" name="sujet" pattern="[A-zÀ-ÖØ-öø-ÿ0-9- ]{2,}" maxlength="50" required />
        <br />
        <label>Message</label>
        <br />
        <textarea class="input" name="message" rows="5" cols="33" pattern="[A-zÀ-ÖØ-öø-ÿ- ]{2,}" maxlength="1000" required></textarea>
        </br> </br>
        <input type="submit" value="Envoyer" id="button" />
        </br> </br>
  </form>

  <?php else :
        //Comment valider les données en provenance de mon formulaire ?

        //1 Les données obligatoires sont elles présentes ?
        if (isset($_POST["mail"]) && isset($_POST["message"]) && isset($_POST["prenom"]) && isset($_POST["nom"]) && isset($_POST["sujet"])) {

          //2 Les données obligatoires sont-elles remplies ?
          if (!empty($_POST["mail"]) && !empty($_POST["message"]) && !empty($_POST["prenom"]) && !empty($_POST["nom"]) && !empty($_POST["sujet"])) {

            //3 Les données obligatoires ont-elles une valeur conforme à la forme attendue ?
            if (filter_var($_POST["mail"], FILTER_SANITIZE_EMAIL)) {

              $_POST["mail"] = filter_var($_POST["mail"], FILTER_SANITIZE_EMAIL);

              if (filter_var($_POST["message"], FILTER_SANITIZE_FULL_SPECIAL_CHARS)) {
                $_POST["message"] = filter_var($_POST["message"], FILTER_SANITIZE_FULL_SPECIAL_CHARS);

                if (filter_var($_POST["prenom"], FILTER_SANITIZE_STRING) && filter_var($_POST["nom"], FILTER_SANITIZE_STRING) && filter_var($_POST["sujet"], FILTER_SANITIZE_STRING)) {
                  $_POST["prenom"] = filter_var($_POST["prenom"], FILTER_SANITIZE_STRING);
                  $_POST["nom"] = filter_var($_POST["nom"], FILTER_SANITIZE_STRING);
                  $_POST["sujet"] = filter_var($_POST["sujet"], FILTER_SANITIZE_STRING);

                  //4 Les données optionnelles sont-elles présentes ? Les données optionnelles sont-elles remplies ? Les données optionnelles ont-elles une valeur conforme à la forme attendue ?                
                  if (isset($_POST["entreprise"])) {
                    $_POST["entreprise"] = filter_var($_POST["entreprise"], FILTER_SANITIZE_STRING);
                  } else {
                    $_POST["entreprise"] = "";
                  }

                  if (isset($_POST["fonction"])) {
                    $_POST["fonction"] = filter_var($_POST["fonction"], FILTER_SANITIZE_STRING);
                  } else {
                    $_POST["fonction"] = "";
                  }

                  if (isset($_POST["telephone"])) {
                    $_POST["telephone"] = filter_var($_POST["telephone"], FILTER_SANITIZE_STRING);
                  } else {
                    $_POST["telephone"] = "";
                  }

                  //5 afficher le mail et le message
  ?>
            <p>Bonjour <?php printf('%s', $_POST["prenom"]); ?>, votre message a été envoyé et vous serez contacté dans les plus bref délais !</p>
            <meta http-equiv="refresh" content="1;URL=#contact">
  <?php
                  //6 utiliser la fonction mail() pour envoyer le message vers botre boîte mail
                  $to = "savinien.monteil@gmail.com";
                  $subject = $_POST["sujet"];
                  $message = 'Vous avez un nouveau message de ' . $_POST["prenom"] . ' ' . $_POST["nom"];
                  $message .= PHP_EOL . 'Contenu du message : ' . $_POST["message"];
                  if ($_POST["entreprise"]) {
                    $message .= PHP_EOL . 'Entreprise : ' . $_POST["entreprise"];
                  }
                  if ($_POST["fonction"]) {
                    $message .= PHP_EOL . 'Fonction dans l\'entreprise : ' . $_POST["fonction"];
                  }
                  if ($_POST["telephone"]) {
                    $message .= PHP_EOL . 'Téléphone : ' . $_POST["telephone"];
                  }
                  if ($_POST["mail"]) {
                    $message .= PHP_EOL . 'Email : ' . $_POST["mail"];
                  }
                  $headers = 'From: ' . 'savinien-monteil@labo-ve.fr' . PHP_EOL .
                    'Reply-To: ' . $_POST["mail"] . PHP_EOL .
                    'Content-Type: text/html; charset=UTF-8' . PHP_EOL .
                    'X-Mailer: PHP/' . phpversion();
                  mail($to, $subject, $message, $headers);
                } else {
                  print("Fournissez des informations valides svp !");
                }
              } else {
                print("La forme de votre message a outrepassé toutes nos prédictions !");
              }
            } else {
              print("Saisir une adresse email valide !");
            }
          } else {
            print("Il faut saisir les valeurs obligatoires !");
          }
        } else {
          print("RENDS-MOI MES CODES PETIT DÉGLINGO !");
        }
  ?>
<?php
      endif;
?>
</div>

<!-- Sources

Pour l'effet particules dans le cadre
 - Télécharger le doc github :https://vincentgarreau.com/particles.js/2
 - Le petit tuto qui va bien :https://www.youtube.com/watch?v=cUzihD4JBQU&t=6s3 
 - Pour modifier l'aspect, la vitesse, les links, etc. : aller sur l'app.jsPour l'effet de gradient :https://sarcadass.github.io/granim.js/examples.html
 - Pour le carroussel : https://kenwheeler.github.io/slick/?ref=blogduwebdesign.comEffets
 - hover du turfu :http://ianlunn.github.io/Hover/?ref=blogduwebdesign.comDifférentes 
 - anims :
  https://www.minimamente.com/project/magic/?ref=blogduwebdesign.com
 - Effet de ligne qui se trace (si possible) :https://maxwellito.github.io/vivus/?ref=blogduwebdesign.com
 - Gradient https://uigradients.com/#RainbowBlue -->


<script src="library/jquery/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="library/owl.carousel/owl.carousel.min.js"></script>
<script src="script.js"></script>
<script src="library/aos/aos.js"></script>

</body>

</html>