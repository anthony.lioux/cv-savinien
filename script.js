jQuery(document).ready(function($){

    $('.profil').on({
         'click': function(){
             $('#change-image').attr('src','img/Rick-astley.webp');
         }
     });

    document.getElementById('play').addEventListener('click', function (e) {
        e.preventDefault();
        document.getElementById('audio').play();
      });
      
AOS.init();

let navbar = document.querySelector(".navbar")
let ham = document.querySelector(".ham")

ham.addEventListener("click", toggleHamburger)

function toggleHamburger(){
    navbar.classList.toggle("showNav")
    ham.classList.toggle("showClose")
  }
  
  let menuLinks = document.querySelectorAll(".menuLink")

  menuLinks.forEach( 
    function(menuLink) { 
      menuLink.addEventListener("click", toggleHamburger) 
    }
  )

$("#button").click(function() {
    $([document.documentElement, document.body]).animate({
        scrollTop: $("#contact").offset().top
    }, 2000);
});

/* carousel */

$('.owl-carousel').owlCarousel({
  loop:true,
  margin:10,
  autoplay:true,
  autoplayTimeout:3000,
  responsive:{
      0:{
          items:1
      },
      600:{
          items:2
      },
      1000:{
          items:3
      }
  }
})
});